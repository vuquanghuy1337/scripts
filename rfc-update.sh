#!/bin/bash
git checkout -b rfc-2023$1_init
a=( \
`cat changes-in-rfc.txt`
)
for (( i=0; i<${#a[@]}-1; i=i+3 )) ; 
do
	sed -i 's/version:.*/version: \"'${a[i+2]}'\"/g' charts/${a[i]}/${a[i+1]}/values-production.yaml
	export APP=${a[i]}/${a[i+1]}
	export VER=${a[i+2]}
	git add charts/${a[i]}/${a[i+1]}/values-production.yaml && git commit -m "production/$APP: Update image version to $VER"
	git push --set-upstream origin rfc-2023$1_init
done
git add changes-in-rfc.txt && git commit -m "Push for pulling when golive"
git push
let x=${#a[@]} y=3 total=x/y
let commit=$(git rev-list --count HEAD  --since=2.minute)
let appUpdate=commit-1
echo -e "\033[0;32m"${appUpdate}/${total} applications has been updated version successfully!!!

# README.md
# Firstly, define array in changes-in-rfc.txt with format project application image-version, example:
# bankgateway     bank-balance-tool   v1.0.0-999-bank-balance-tool
# paymentgateway  insurance           v1.0.0-999-insurance
# Run the script with the format rfc-update.sh month-date, example:
# 	rfc-update.sh 0308
# It will push all commit to new branch rfc-20230308_init
