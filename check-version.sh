#!/bin/bash

read_rfc_changes() {
content=$(cat "$1")
start_marking=false
project=""
content=$(echo "$content" | sed '/^\s*$/d')
while IFS= read -r line; do
    if [[ $line =~ ^[[:alpha:]] ]]; then
        if [ "$start_marking" = true ]; then
            project="$line"
            start_marking=false
        fi
    else
        if [ "$start_marking" = true ]; then
            echo "$project $line"
        fi
    fi

    if [[ $line =~ ^[[:alpha:]] ]]; then
        start_marking=true
    fi
done <<< "$content" > content.txt
total_app=$(wc -l < content.txt)
mark_array=()
for ((i=0; i<$total_app; i++)); do
    mark_array+=("0")
done
printf "%s\n" "${mark_array[@]}" > mark_array.txt
}

checking() {
a=( \
`cat content.txt`
)
GREEN='\033[0;32m'
RED='\033[0;31m' 
OFF='\033[0m' 
mark_array=( $(cat mark_array.txt) )
total_app=$(wc -l < mark_array.txt)
    for (( i=0; i<${#a[@]}-1; i=i+3 )) ; do
        j=$((i / 3))
        if [ "${mark_array[j]}" == "0" ] ; then
            current_version=$(kubectl get pod -n "${a[i]}-production" | grep "${a[i+1]}" | cut -d ' ' -f1 | head -n1 | xargs kubectl get pod -n ${a[i]}-production -o jsonpath="{.status.containerStatuses[0].image}" | sed 's/.*-g//g')
            desired_version=$(echo -e "${a[i+2]}")
            container_status=$(kubectl get pod -n "${a[i]}-production" | grep "${a[i+1]}" | cut -d ' ' -f1 | head -n1 | xargs kubectl get pod -n ${a[i]}-production -o jsonpath="{.status.phase}")
            container_ready=$(kubectl get pod -n "${a[i]}-production" | grep "${a[i+1]}" | cut -d ' ' -f1 | head -n1 | xargs kubectl get pod -n ${a[i]}-production -o jsonpath="{.status.containerStatuses[0].ready}")
            
            if [ "${current_version:0:7}" != "${desired_version:0:7}" ] ; then 
                echo -e "${GREEN}${a[i]}/${a[i+1]}:${OFF} The desired version is ${GREEN}${desired_version}${OFF} but the current version is ${RED}${current_version}${OFF}"
            elif [ "$container_status" != "Running" ] ; then
                echo -e "${GREEN}${a[i]}/${a[i+1]}:${OFF} Container status is ${RED}${container_status}${OFF}"
            elif [ "$container_ready" != "true" ] ; then
                echo -e "${GREEN}${a[i]}/${a[i+1]}:${OFF} Container is not ready, the ready status is ${RED}${container_ready}${OFF}"
            else
                line_number=$((j+1))
                echo -e "${GREEN}${a[i]}/${a[i+1]}${OFF} is running the correct version" && sed -i "${line_number}s/0/1/" mark_array.txt
            fi
        fi
    done

echo -e "\e[47m\e[30mSuccessful apps are $(grep -o '1' mark_array.txt | wc -l)/$total_app apps\e[0m"
}

if [ -z "$1" ]; then
    echo "Rechecking"
    checking
else
    echo "First time checking"
    read_rfc_changes "$1"
    checking
fi

=================
# bash rfc-script.sh <filename>
# VD: bash rfc-script.sh 20231108

# or

# ./rfc-script.sh
# VD: ./rfc-script.sh 20231108
================
# Check-version script:
# Copy 3 cột đầu tiên vào file yyyymmdd (không xóa dòng đầu):
# Copy first 3 columns and paste them below:
# bankgateway
# gateway-va	a239196
# paymentgateway
# payment-service-v3	c40bbe65
# payment-service-v4	18b978c8
# Chạy lần đầu: check-version-update.sh rfc_change_list/yyyymmdd
# Chạy các lần sau: check-version-update.sh
# Note:

# Script tạo 1 array ghi ra file mark_array.txt với toàn bộ phần tử 0 ứng với mỗi app
# Khi 1 app đã chạy đúng version trong file RFC, đánh dấu giá trị 1 để không recheck version
